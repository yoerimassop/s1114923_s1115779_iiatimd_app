package com.example.s1114923_s1115779_iiatimd_app;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class ViewDescriptionActivity extends AppCompatActivity {

    private String description, recipeTitle;
    private TextView descriptionTxtView, title;
    private ImageView postImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_description);
        init();
    }

    private void init() {
        description = getIntent().getStringExtra("description");
        descriptionTxtView = findViewById(R.id.txtDescriptionView);
        recipeTitle = getIntent().getStringExtra("title");
        title = findViewById(R.id.recipeTitle);
        title.setText(recipeTitle);
        descriptionTxtView.setText(description);
        postImg = findViewById(R.id.imgPostDescription);
        Picasso.get().load(Constant.URL+"storage/posts/"+  getIntent().getStringExtra("photo")).into(postImg);
    }

    public void cancelDescription(View view) {
        super.onBackPressed();
    }
}
