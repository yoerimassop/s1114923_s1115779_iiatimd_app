package com.example.s1114923_s1115779_iiatimd_app;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.example.s1114923_s1115779_iiatimd_app.DAO.CommentDAO;
import com.example.s1114923_s1115779_iiatimd_app.DAO.PostDAO;
import com.example.s1114923_s1115779_iiatimd_app.Models.Comment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

@Database(entities = {Comment.class, Post.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public abstract PostDAO postDAO();
    public abstract CommentDAO commentDAO();

    public static synchronized AppDatabase getInstance(Context context){
        if(instance == null){
            instance = create(context);
        }
        return instance;
    }

    private static AppDatabase create(final Context context){
        return Room.databaseBuilder(context, AppDatabase.class, "RecepAppDB").fallbackToDestructiveMigration().build();
    }
}
