package com.example.s1114923_s1115779_iiatimd_app.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.s1114923_s1115779_iiatimd_app.Adapters.PostsAdapter;
import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.Constant;
import com.example.s1114923_s1115779_iiatimd_app.HomeActivity;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;
import com.example.s1114923_s1115779_iiatimd_app.R;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.GetPostsTask;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.InsertPostTask;
import com.example.s1114923_s1115779_iiatimd_app.VolleySingleton;
import com.google.android.material.appbar.MaterialToolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeFragment extends Fragment {
    private View view;
    public static RecyclerView recyclerView;
    public static ArrayList<Post> arrayList;
    private SwipeRefreshLayout refreshLayout;
    private PostsAdapter postsAdapter;
    private MaterialToolbar toolbar;
    private SharedPreferences sharedPreferences;

    public HomeFragment() {}

    public void processData(ArrayList<Post> posts) {
        ((HomeActivity)getContext()).runOnUiThread(() -> {
            postsAdapter = new PostsAdapter(getContext(), posts);
            recyclerView.setAdapter(postsAdapter);
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_home,container,false);
        init();
        return view;
    }

    private void init(){
        AppDatabase db = AppDatabase.getInstance((getContext()).getApplicationContext());
        sharedPreferences = getContext().getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        recyclerView = view.findViewById(R.id.recyclerHome);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        refreshLayout = view.findViewById(R.id.swipeHome);
        toolbar = view.findViewById(R.id.toolbarHome);

        ((HomeActivity)getContext()).setSupportActionBar(toolbar);
        ((HomeActivity)getContext()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setHasOptionsMenu(true);

        boolean internet = checkConnection();

        if(internet == true) {
            getPosts();
        }else{
            new Thread(new GetPostsTask(db, this)).start();
        }

        HomeFragment homeFragment = this;
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                boolean internet = checkConnection();
                if(internet == true) {
                    getPosts();
                }else{
                    new Thread(new GetPostsTask(db, homeFragment)).start();
                    refreshLayout.setRefreshing(false);
                }
            }
        });
    }

    private void getPosts(){
        refreshLayout.setRefreshing(true);
        arrayList = new ArrayList<>();

        AppDatabase db = AppDatabase.getInstance(getContext().getApplicationContext());

        StringRequest request = new StringRequest(Request.Method.GET, Constant.POSTS,response -> {

            try {
                JSONObject object = new JSONObject(response);
                if (object.getBoolean("success")){

                    JSONArray array = new JSONArray(object.getString("posts"));
                    for (int i = 0; i < array.length(); i++){
                        JSONObject postObject = array.getJSONObject(i);
                        JSONObject userObject = postObject.getJSONObject("user");

                        Post post = new Post();
                        post.setId(postObject.getInt("id"));
                        post.setUserId(userObject.getInt("id"));
                        post.setUserName(userObject.getString("name") + " " +userObject.getString("lastname"));
                        post.setUserPhoto(userObject.getString("photo"));
                        post.setLikes(postObject.getInt("likesCount"));
                        post.setComments(postObject.getInt("commentsCount"));
                        post.setDate(postObject.getString("created_at"));
                        post.setTitle(postObject.getString("recipe_title"));
                        post.setSideDescription(postObject.getString("side_description"));
                        post.setDesc(postObject.getString("recipe_description"));
                        post.setPhoto(postObject.getString("photo"));
                        post.setSelfLike(postObject.getBoolean("selfLike"));

                        arrayList.add(post);
                        new Thread(new InsertPostTask(db, post)).start();
                    }

                    postsAdapter = new PostsAdapter(getContext(),arrayList);
                    recyclerView.setAdapter(postsAdapter);
                }

            }catch(JSONException e) {
                e.printStackTrace();
            }

            refreshLayout.setRefreshing(false);

        },error -> {
            error.printStackTrace();
            refreshLayout.setRefreshing(false);
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = sharedPreferences.getString("token","");
                HashMap<String,String> map = new HashMap<>();
                map.put("Authorization","Bearer " +token);
                return map;
            }
        };

        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                postsAdapter.getFilter().filter(newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void showPosts(ArrayList<Post> posts) {
        ((HomeActivity)getContext()).runOnUiThread(() -> {
            postsAdapter = new PostsAdapter(getContext(), posts);
            recyclerView.setAdapter(postsAdapter);
        });
    }

    boolean checkConnection(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }
}
