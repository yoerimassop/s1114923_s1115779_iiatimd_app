package com.example.s1114923_s1115779_iiatimd_app.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.CommentActivity;
import com.example.s1114923_s1115779_iiatimd_app.Constant;
import com.example.s1114923_s1115779_iiatimd_app.EditPostActivity;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;
import com.example.s1114923_s1115779_iiatimd_app.R;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.DeletePostTask;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.UpdatePostTask;
import com.example.s1114923_s1115779_iiatimd_app.ViewDescriptionActivity;
import com.example.s1114923_s1115779_iiatimd_app.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostsHolder>{

    private final Context context;
    private final ArrayList<Post> list;
    private final ArrayList<Post> listAll;
    private final SharedPreferences preferences;

    public PostsAdapter(Context context, ArrayList<Post> list) {
        this.context = context;
        this.list = list;
        this.listAll = new ArrayList<>(list);
        preferences = context.getApplicationContext().getSharedPreferences("user",Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public PostsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_post,parent,false);
        return new PostsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostsHolder holder, int position) {
        Post post = list.get(position);
        Picasso.get().load(Constant.URL+"storage/profiles/"+post.getUserPhoto()).into(holder.imgProfile);
        Picasso.get().load(Constant.URL+"storage/posts/"+post.getPhoto()).into(holder.imgPost);
        holder.txtName.setText(post.getUserName());
        holder.txtComments.setText("Bekijk alle "+post.getComments() + " reacties");
        holder.txtLikes.setText(post.getLikes()+" Likes");
        holder.txtDate.setText(post.getDate());
        holder.txtTitle.setText(post.getTitle());
        holder.txtSideDescription.setText(post.getSideDescription());
        AppDatabase db = AppDatabase.getInstance(context.getApplicationContext());


        holder.btnLike.setImageResource(
                post.isSelfLike()?R.drawable.ic_baseline_favorite_24:R.drawable.ic_baseline_favorite_border_24
        );

        holder.btnLike.setOnClickListener(v -> {
            boolean internet = checkConnection();
            if(internet == true) {
                holder.btnLike.setImageResource(
                    post.isSelfLike()?R.drawable.ic_baseline_favorite_border_24:R.drawable.ic_baseline_favorite_24
                );

                StringRequest request = new StringRequest(Request.Method.POST,Constant.LIKE_POST, response -> {
                    Post mPost = list.get(position);
                    try {
                        JSONObject object = new JSONObject(response);

                        if(object.getString("message").equals("liked")){
                            String notificationRoute = Constant.LIKE_NOTIFICATION + "/" + mPost.getUserId();
                            StringRequest likeRequest = new StringRequest(Request.Method.POST,notificationRoute , res -> {
                            }, err->{
                                err.printStackTrace();
                            }){
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    String token = preferences.getString("token", "");
                                    HashMap<String,String> map = new HashMap<>();
                                    map.put("Authorization", "Bearer " + token);
                                    return map;
                                }

                                };

                            RequestQueue queueLike = Volley.newRequestQueue(context);
                            queueLike.add(likeRequest);
                        }
                        if(object.getBoolean("success")){
                            mPost.setSelfLike(!post.isSelfLike());
                            mPost.setLikes(mPost.isSelfLike() ?post.getLikes()+1:post.getLikes()-1);

                            new Thread(new UpdatePostTask(db, mPost)).start();
                            list.set(position, mPost);
                            notifyItemChanged(position);
                            notifyDataSetChanged();
                        }
                        else{
                            holder.btnLike.setImageResource(
                                    post.isSelfLike()?R.drawable.ic_baseline_favorite_24:R.drawable.ic_baseline_favorite_border_24
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, err->{
                    err.printStackTrace();
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        String token = preferences.getString("token", "");
                        HashMap<String,String> map = new HashMap<>();
                        map.put("Authorization", "Bearer " + token);
                        return map;
                    }

                    @Nullable
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String,String> map = new HashMap<>();
                        map.put("id", post.getId()+"");
                        return map;
                    }
                };

                VolleySingleton.getInstance(context).addToRequestQueue(request);
            }else{
                Toast.makeText(context, "Verbind met internet", Toast.LENGTH_SHORT).show();
            }
        });
        if(post.getUserId()==preferences.getInt("id",0)){
            holder.btnPostOption.setVisibility(View.VISIBLE);
        } else{
            holder.btnPostOption.setVisibility(View.GONE);
        }

        holder.btnPostOption.setOnClickListener(v->{
            PopupMenu popupMenu = new PopupMenu(context,holder.btnPostOption);
            popupMenu.inflate(R.menu.menu_post_options);
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    switch (menuItem.getItemId()){
                        case R.id.item_edit: {
                            Intent i = new Intent(context, EditPostActivity.class);
                            i.putExtra("postId", post.getId());
                            i.putExtra("position", position);
                            i.putExtra("title", post.getTitle());
                            i.putExtra("description", post.getDesc());
                            i.putExtra("side_description", post.getSideDescription());
                            i.putExtra("image", Constant.URL + "storage/posts/" + post.getPhoto());
                            context.startActivity(i);
                            return true;
                        }
                        case R.id.item_delete: {
                            boolean internet = checkConnection();
                            if(internet == true) {
                                deletePost(post, post.getId(), position);
                            }else{
                                Toast.makeText(v.getContext(), "Verbind met internet", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    return false;
                }
            });
            popupMenu.show();
        });

        holder.txtComments.setOnClickListener(v->{
            Intent i = new Intent(context, CommentActivity.class);
            i.putExtra("postId", post.getId());
            i.putExtra("postPosition",position);
            context.startActivity(i);
        });

        holder.btnComment.setOnClickListener(v->{
            Intent i = new Intent(context, CommentActivity.class);
            i.putExtra("postId", post.getId());
            i.putExtra("postPosition",position);
            context.startActivity(i);
        });

        holder.btnDescription.setOnClickListener(v->{
            Intent i = new Intent(context, ViewDescriptionActivity.class);
            i.putExtra("description", post.getDesc());
            i.putExtra("photo", post.getPhoto());
            i.putExtra("title", post.getTitle());
            context.startActivity(i);


        });
    }

    private void deletePost(Post post, int postId, int position){
        AppDatabase db = AppDatabase.getInstance(context.getApplicationContext());

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Bevestig");
        builder.setMessage("Verwijder Post?");
        builder.setPositiveButton("Verwijder", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                StringRequest request = new StringRequest(Request.Method.POST,Constant.DELETE_POST, response ->{
                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getBoolean("success")){
                            list.remove(position);
                            notifyItemRemoved(position);
                            notifyDataSetChanged();
                            listAll.clear();
                            listAll.addAll(list);

                            new Thread(new DeletePostTask(db, post)).start();
                            Toast.makeText(context, "Succesvol verwijderd", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(context, "Verwijderen mislukt", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }, error-> {
                    error.printStackTrace();
                    Toast.makeText(context, "Verwijderen mislukt", Toast.LENGTH_SHORT).show();
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        String token = preferences.getString("token", "");
                        HashMap<String, String> map = new HashMap<>();
                        map.put("Authorization", "Bearer " + token);
                        return map;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("id",postId+"");
                        return map;
                    }
                };

                VolleySingleton.getInstance(context).addToRequestQueue(request);
            }
        });
        builder.setNegativeButton("Annuleer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            ArrayList<Post> filteredList = new ArrayList<>();
            if (constraint.toString().isEmpty()){
                filteredList.addAll(listAll);
            }else{
                for (Post post : listAll ){
                    if (post.getTitle().toLowerCase().contains(constraint.toString().toLowerCase()) || post.getUserName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(post);
                    }
                }

            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list.clear();
            list.addAll((Collection<? extends Post>) results.values);
            notifyDataSetChanged();
        }
    };

    public Filter getFilter(){
        return filter;
    }

    class PostsHolder extends RecyclerView.ViewHolder{

        private final TextView txtName;
        private final TextView txtDate;
        private final TextView txtTitle;
        private final TextView txtSideDescription;
        private final TextView txtLikes;
        private final TextView txtComments;
        private final CircleImageView imgProfile;
        private final ImageView imgPost;
        private final ImageButton btnPostOption;
        private final ImageButton btnLike;
        private final ImageButton btnComment;
        private final Button btnDescription;

        public PostsHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtPostName);
            txtDate = itemView.findViewById(R.id.txtPostDate);
            txtTitle = itemView.findViewById(R.id.txtPostTitle);
            txtSideDescription = itemView.findViewById(R.id.txtPostSideDescription);
            txtLikes = itemView.findViewById(R.id.txtPostLikes);
            txtComments = itemView.findViewById(R.id.txtPostComments);
            imgProfile = itemView.findViewById(R.id.imgPostProfile);
            imgPost = itemView.findViewById(R.id.imgPostPhoto);
            btnPostOption = itemView.findViewById(R.id.btnPostOption);
            btnLike = itemView.findViewById(R.id.btnPostLike);
            btnComment = itemView.findViewById(R.id.btnPostComment);
            btnDescription = itemView.findViewById(R.id.ingredientsButton);
            btnPostOption.setVisibility(View.GONE);
        }
    }
    boolean checkConnection(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }
}
