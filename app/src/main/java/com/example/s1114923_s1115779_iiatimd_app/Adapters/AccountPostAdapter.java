package com.example.s1114923_s1115779_iiatimd_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.s1114923_s1115779_iiatimd_app.Constant;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;
import com.example.s1114923_s1115779_iiatimd_app.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AccountPostAdapter extends RecyclerView.Adapter<AccountPostAdapter.AccountPostHolder> {
    private final Context context;
    private final ArrayList<Post> arrayList;
    public AccountPostAdapter(Context context, ArrayList<Post> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public AccountPostAdapter.AccountPostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_account_post,parent,false);
        return new AccountPostHolder(v);
    }

    public void onBindViewHolder(@NonNull AccountPostHolder holder, int position){
        Post post = arrayList.get(position);
        Picasso.get().load(Constant.URL+"storage/posts/"+post.getPhoto()).into(holder.imageView);
    }

    public int getItemCount(){return arrayList.size();}


    class AccountPostHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;

        public AccountPostHolder(@NonNull View itemView){
            super(itemView);
            imageView = itemView.findViewById(R.id.imgAccountPost);
        }

    }
}
