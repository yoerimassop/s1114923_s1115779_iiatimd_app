package com.example.s1114923_s1115779_iiatimd_app.Adapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.CommentActivity;
import com.example.s1114923_s1115779_iiatimd_app.Constant;
import com.example.s1114923_s1115779_iiatimd_app.Fragments.HomeFragment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Comment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;
import com.example.s1114923_s1115779_iiatimd_app.R;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.DeleteCommentTask;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.InsertCommentTask;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.UpdatePostTask;
import com.example.s1114923_s1115779_iiatimd_app.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsHolder> {
    private final Context context;
    private final ArrayList<Comment> list;
    private final SharedPreferences preferences;
    private final ProgressDialog dialog;


    public CommentsAdapter(Context context, ArrayList<Comment> list) {
        this.context = context;
        this.list = list;
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        preferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public CommentsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_comment, parent, false);
        return new CommentsHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsHolder holder, int position) {
        Comment comment = list.get(position);
        Picasso.get().load(comment.getUserPhoto()).into(holder.imgProfile);
        holder.txtName.setText(comment.getUserName());
        holder.txtDate.setText(comment.getDate());
        holder.txtComment.setText(comment.getComment());

        if (preferences.getInt("id", 0)!=comment.getUserId()){
            holder.btnDelete.setVisibility(View.GONE);
        }
        else{
            holder.btnDelete.setVisibility(View.VISIBLE);
            holder.btnDelete.setOnClickListener(v->{
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Weet je het zeker?");
                builder.setPositiveButton("Verwijderen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        boolean internet = checkConnection();
                        if(internet == true) {
                            boolean online = preferences.getBoolean("online", true);
                            if (online == true) {
                                deleteComment(comment.getId(), position);
                            } else {
                                Toast.makeText(v.getContext(), "Laad de pagina opnieuw", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(v.getContext(), "Verbind met internet", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                builder.setNegativeButton("Annuleren", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();
            });
        }
    }

    private void deleteComment(int commentId,int position) {
        AppDatabase db = AppDatabase.getInstance(context.getApplicationContext());
        dialog.setMessage("Verwijder reactie");
        dialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.DELETE_COMMENT, response ->{
            try {
                JSONObject object = new JSONObject(response);
                if (object.getBoolean("success")){
                    Comment comment = list.get(position);
                    new Thread(new DeleteCommentTask(db, comment)).start();

                    list.remove(position);
                    Post post = HomeFragment.arrayList.get(CommentActivity.postPosition);
                    post.setComments(post.getComments()-1);
                    HomeFragment.arrayList.set(CommentActivity.postPosition,post);
                    HomeFragment.recyclerView.getAdapter().notifyDataSetChanged();
                    notifyDataSetChanged();
                    new Thread(new UpdatePostTask(db, post)).start();
                    Toast.makeText(context, "Succesvol verwijderd", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(context, "Verwijderen mislukt", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            dialog.dismiss();
        }, error-> {
            error.printStackTrace();
            dialog.dismiss();
            Toast.makeText(context, "Verwijderen mislukt", Toast.LENGTH_SHORT).show();
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("id",commentId+"");
                return map;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(request);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CommentsHolder extends RecyclerView.ViewHolder{
        private final CircleImageView imgProfile;
        private final TextView txtName;
        private final TextView txtDate;
        private final TextView txtComment;
        private final ImageButton btnDelete;
        public CommentsHolder(@NonNull View itemView) {
            super(itemView);
            imgProfile = itemView.findViewById(R.id.imgCommentProfile);
            txtName = itemView.findViewById(R.id.txtCommentName);
            txtDate = itemView.findViewById(R.id.txtCommentDate);
            txtComment = itemView.findViewById(R.id.txtCommentText);
            btnDelete = itemView.findViewById(R.id.btnDeleteComment);
        }

    }

    boolean checkConnection(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }
}
