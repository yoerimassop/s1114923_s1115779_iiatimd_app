package com.example.s1114923_s1115779_iiatimd_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.s1114923_s1115779_iiatimd_app.R;

public class ViewPagerAdapter extends PagerAdapter {

    private final Context context;
    private LayoutInflater inflater;

    public ViewPagerAdapter(Context context) {
        this.context = context;
    }

    private final int[] images = {
            R.drawable.inspire,
            R.drawable.learn,
            R.drawable.enjoy
    };

    private final String[] titles = {
        "Inspireer!",
        "Deel!",
        "Geniet!"
    };

    private final String[] descs = {
            "Weet je niet wat je vanavond wilt eten? Laat je dan inspireren door de heerlijke recepten van medegebruikers van deze app!",
            "Deel jouw overheerlijk klaargemaakte recepten met andere gebruikers. Ontvang zoveel mogelijk likes en reageer op reacties van andere gebruikers.",
            "Bekijk de ingrediënten en bereidingswijze van verschillende gerechten. Haal de ingredieënten in de supermarkt, bereidt het gerecht thuis en geniet van het lekkere eten!"
    };

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.view_pager,container,false);

        ImageView imageView = v.findViewById(R.id.imgViewPager);
        TextView txtTitle = v.findViewById(R.id.txtTitleViewPager);
        TextView txtDesc = v.findViewById(R.id.txtDescViewPager);

        imageView.setImageResource(images[position]);
        txtTitle.setText(titles[position]);
        txtDesc.setText(descs[position]);

        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
