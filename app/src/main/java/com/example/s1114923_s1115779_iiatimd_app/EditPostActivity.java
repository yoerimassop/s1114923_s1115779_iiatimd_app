package com.example.s1114923_s1115779_iiatimd_app;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.s1114923_s1115779_iiatimd_app.Fragments.HomeFragment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.UpdatePostTask;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EditPostActivity extends AppCompatActivity {

    private int position = 0, id=0;
    private ImageView imageView;
    private EditText txtDesc;
    private EditText txtSideDescription;
    private EditText txtTitle;
    private Button btnSave;

    private Bitmap bitmap = null;
    private static final int GALLERY_CHANGE_POST = 3;
    private ProgressDialog dialog;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);
        init();
    }

    private void init() {
        sharedPreferences = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        txtDesc = findViewById(R.id.txtDescEditPost);
        txtSideDescription = findViewById(R.id.txtSideDescriptionEditPost);
        txtTitle = findViewById(R.id.txtTitleEditPost);
        btnSave = findViewById(R.id.btnEditPost);
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        position = getIntent().getIntExtra("position", 0);
        id = getIntent().getIntExtra("postId", 0);
        imageView = findViewById(R.id.imgEditPost);

        Picasso.get().load(getIntent().getStringExtra("image")).into(imageView);
        txtDesc.setText(getIntent().getStringExtra("description"));
        txtTitle.setText(getIntent().getStringExtra("title"));
        txtSideDescription.setText(getIntent().getStringExtra("side_description"));

        btnSave.setOnClickListener(v->{
            if(!txtTitle.getText().toString().isEmpty() && !txtDesc.getText().toString().isEmpty() && !txtSideDescription.getText().toString().isEmpty()){
                boolean internet = checkConnection();
                if(internet == true) {
                    updatePost();
                }else{
                    Toast.makeText(this, "Verbind met internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updatePost()
    {
        AppDatabase db = AppDatabase.getInstance(this.getApplicationContext());
        dialog.setMessage("Opslaan...");
        dialog.show();

        StringRequest request = new StringRequest(Request.Method.POST,Constant.UPDATE_POST, response ->{
            try {
                JSONObject object = new JSONObject(response);
                if (object.getBoolean("success")){
                    JSONObject postObject = object.getJSONObject("post");

                    Post post = HomeFragment.arrayList.get(position);
                    post.setTitle(txtTitle.getText().toString());
                    post.setSideDescription(txtSideDescription.getText().toString());
                    post.setDesc(txtDesc.getText().toString());
                    post.setPhoto(postObject.getString("photo"));
                    HomeFragment.arrayList.set(position,post);
                    HomeFragment.recyclerView.getAdapter().notifyItemChanged(position);
                    HomeFragment.recyclerView.getAdapter().notifyDataSetChanged();
                    Toast.makeText(this, "Succesvol gewijzigd", Toast.LENGTH_SHORT).show();

                    new Thread(new UpdatePostTask(db, post)).start();
                    finish();
                }
            } catch (JSONException e) {
                Toast.makeText(this, "Opslaan mislukt", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            dialog.dismiss();
        }, error-> {
            Toast.makeText(this, "Opslaan mislukt", Toast.LENGTH_SHORT).show();
            error.printStackTrace();
            dialog.dismiss();
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = sharedPreferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("id", id+"");
                map.put("title", txtTitle.getText().toString().trim());
                map.put("side_description", txtSideDescription.getText().toString().trim());
                map.put("description", txtDesc.getText().toString().trim());
                map.put("photo", bitmapToString(bitmap));
                return map;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private String bitmapToString(Bitmap bitmap) {
        if (bitmap!=null){
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte [] array = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(array, Base64.DEFAULT);
        }
        return "";
    }

    public void cancelEdit(View view) {
        super.onBackPressed();
    }

    public void changePhoto(View view) {
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i, GALLERY_CHANGE_POST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_CHANGE_POST && resultCode == RESULT_OK){
            Uri imgUri = data.getData();
            imageView.setImageURI(imgUri);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    boolean checkConnection(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }
}