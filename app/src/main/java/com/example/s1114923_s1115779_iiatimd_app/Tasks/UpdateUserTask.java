package com.example.s1114923_s1115779_iiatimd_app.Tasks;

import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.EditUserInfoActivity;
import com.example.s1114923_s1115779_iiatimd_app.Models.Comment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

import java.util.ArrayList;

public class UpdateUserTask implements Runnable {

    EditUserInfoActivity editUserInfoActivity;
    AppDatabase db;
    int userId;

    public UpdateUserTask(AppDatabase db, EditUserInfoActivity editUserInfoActivity, int userId){
        this.db = db;
        this.editUserInfoActivity = editUserInfoActivity;
        this.userId = userId;
    }

    @Override
    public void run() {
        ArrayList<Post> posts = (ArrayList<Post>) db.postDAO().getUserPosts(userId);
        ArrayList<Comment> comments = (ArrayList<Comment>) db.commentDAO().getUserComments(userId);
        editUserInfoActivity.updateUser(posts, comments);
    }
}