package com.example.s1114923_s1115779_iiatimd_app.Tasks;

import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

public class UpdatePostTask implements Runnable {

    AppDatabase db;
    Post post;

    public UpdatePostTask(AppDatabase db, Post post){
        this.db = db;
        this.post = post;
    }

    @Override
    public void run() {
        db.postDAO().updatePost(this.post);
    }
}