package com.example.s1114923_s1115779_iiatimd_app.Tasks;

import android.content.SharedPreferences;
import android.util.Log;

import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.Fragments.HomeFragment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

import java.util.ArrayList;
import java.util.List;

public class GetPostsTask implements Runnable {

    HomeFragment homeFragment;
    AppDatabase db;

    public GetPostsTask(AppDatabase db, HomeFragment homeFragment){
        this.db = db;
        this.homeFragment = homeFragment;
    }

    @Override
    public void run() {
        ArrayList<Post> posts = (ArrayList<Post>) db.postDAO().getAll();
        homeFragment.showPosts(posts);
    }
}
