package com.example.s1114923_s1115779_iiatimd_app.Tasks;

import android.util.Log;

import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

import java.util.ArrayList;

public class InsertPostTask implements Runnable {

    AppDatabase db;
    Post post;

    public InsertPostTask(AppDatabase db, Post post){
        this.db = db;
        this.post = post;
    }

    @Override
    public void run() {
        db.postDAO().insertPost(this.post);
    }
}
