package com.example.s1114923_s1115779_iiatimd_app.Tasks;

import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.Models.Comment;

public class DeleteCommentTask implements Runnable {

    AppDatabase db;
    Comment comment;

    public DeleteCommentTask(AppDatabase db, Comment comment){
        this.db = db;
        this.comment = comment;
    }

    @Override
    public void run() {
        db.commentDAO().deleteComment(this.comment);
    }
}
