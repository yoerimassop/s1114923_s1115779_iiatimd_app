package com.example.s1114923_s1115779_iiatimd_app.Tasks;

import android.util.Log;

import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.Models.Comment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

import java.util.ArrayList;

public class InsertCommentTask implements Runnable {

    AppDatabase db;
    Comment comment;

    public InsertCommentTask(AppDatabase db, Comment comment){
        this.db = db;
        this.comment = comment;
    }

    @Override
    public void run() {
        db.commentDAO().insertComment(this.comment);
    }
}