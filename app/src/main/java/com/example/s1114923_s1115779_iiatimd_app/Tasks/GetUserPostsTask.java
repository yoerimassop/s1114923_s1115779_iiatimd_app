package com.example.s1114923_s1115779_iiatimd_app.Tasks;

import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.Fragments.AccountFragment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

import java.util.ArrayList;

public class GetUserPostsTask implements Runnable {

    AccountFragment accountFragment;
    AppDatabase db;
    int userId;

    public GetUserPostsTask(AppDatabase db, AccountFragment accountFragment, int userId){
        this.db = db;
        this.accountFragment = accountFragment;
        this.userId = userId;
    }

    @Override
    public void run() {
        ArrayList<Post> posts = (ArrayList<Post>) db.postDAO().getUserPosts(userId);
        accountFragment.showUserPosts(posts);
    }
}
