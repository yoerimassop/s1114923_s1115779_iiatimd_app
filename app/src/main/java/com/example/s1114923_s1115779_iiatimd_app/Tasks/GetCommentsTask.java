package com.example.s1114923_s1115779_iiatimd_app.Tasks;

import com.example.s1114923_s1115779_iiatimd_app.AppDatabase;
import com.example.s1114923_s1115779_iiatimd_app.CommentActivity;
import com.example.s1114923_s1115779_iiatimd_app.Models.Comment;

import java.util.ArrayList;

public class GetCommentsTask implements Runnable {

    CommentActivity commentActivity;
    AppDatabase db;
    int postId;

    public GetCommentsTask(AppDatabase db, CommentActivity commentActivity, int postId){
        this.db = db;
        this.commentActivity = commentActivity;
        this.postId = postId;
    }

    @Override
    public void run() {
        ArrayList<Comment> comments = (ArrayList<Comment>) db.commentDAO().getPostComments(postId);
        commentActivity.showComments(comments);
    }
}