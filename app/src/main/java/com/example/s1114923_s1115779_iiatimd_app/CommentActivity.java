package com.example.s1114923_s1115779_iiatimd_app;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.s1114923_s1115779_iiatimd_app.Adapters.CommentsAdapter;
import com.example.s1114923_s1115779_iiatimd_app.Fragments.HomeFragment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Comment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.GetCommentsTask;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.InsertCommentTask;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.UpdatePostTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommentActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Comment> list;
    private CommentsAdapter adapter;
    private TextView txtOfflineComments;
    private int postId = 0;
    public static int postPosition = 0;
    private SharedPreferences preferences;
    private EditText txtAddComment;
    private ProgressDialog dialog;
    private boolean online;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        init();
    }

    private void init() {
        AppDatabase db = AppDatabase.getInstance(this.getApplicationContext());
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        postPosition = getIntent().getIntExtra("postPosition", -1);
        preferences = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        recyclerView = findViewById(R.id.recyclerComments);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        txtAddComment = findViewById(R.id.txtAddComment);
        txtOfflineComments = findViewById(R.id.txtOfflineComments);
        postId = getIntent().getIntExtra("postId", 0);
        boolean internet = checkConnection();
        if(internet == true) {
            getComments();
        }else{
            new Thread(new GetCommentsTask(db, this, postId)).start();
        }
    }

    private void getComments() {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("user", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("online", true);
        editor.apply();
        AppDatabase db = AppDatabase.getInstance(this.getApplicationContext());
        list = new ArrayList<>();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.COMMENTS, res->{
            try {
                JSONObject object = new JSONObject(res);
                if (object.getBoolean("success")){
                    JSONArray comments = new JSONArray(object.getString("comments"));
                    for (int i = 0; i < comments.length(); i++){
                        JSONObject comment = comments.getJSONObject(i);
                        JSONObject user = comment.getJSONObject("user");

                        Comment mComment = new Comment();
                        mComment.setId(comment.getInt("id"));
                        mComment.setPostId(postId);
                        mComment.setUserId(user.getInt("id"));
                        mComment.setUserPhoto(Constant.URL + "storage/profiles/" + user.getString("photo"));
                        mComment.setUserName(user.getString("name") + " " + user.getString("lastname"));
                        mComment.setDate(comment.getString("created_at"));
                        mComment.setComment(comment.getString("comment"));

                        new Thread(new InsertCommentTask(db, mComment)).start();
                        list.add(mComment);
                    }
                }

                adapter = new CommentsAdapter(this, list);
                recyclerView.setAdapter(adapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> {
            error.printStackTrace();
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("id", postId+"");
                return map;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(request);
        }


    public void goBack(View view)
    {
        super.onBackPressed();
    }

    public void addComment(View view) {
        if(!txtAddComment.getText().toString().isEmpty()) {
            boolean internet = checkConnection();
            if (internet == true) {
                boolean online = preferences.getBoolean("online", true);
                if (online == true) {
                    AppDatabase db = AppDatabase.getInstance(this.getApplicationContext());
                    String commentText = txtAddComment.getText().toString();
                    dialog.setMessage("Toevoegen reactie");
                    dialog.show();
                    if (commentText.length() > 0) {
                        StringRequest request = new StringRequest(Request.Method.POST, Constant.CREATE_COMMENT, res -> {
                            try {
                                JSONObject object = new JSONObject(res);
                                if (object.getBoolean("success")) {
                                    JSONObject comment = object.getJSONObject("comment");
                                    JSONObject user = comment.getJSONObject("user");

                                    Comment c = new Comment();
                                    c.setPostId(postId);
                                    c.setUserId(user.getInt("id"));
                                    c.setUserPhoto(Constant.URL + "storage/profiles/" + user.getString("photo"));
                                    c.setUserName(user.getString("name") + " " + user.getString("lastname"));
                                    c.setId(comment.getInt("id"));
                                    c.setDate(comment.getString("created_at"));
                                    c.setComment(comment.getString("comment"));

                                    new Thread(new InsertCommentTask(db, c)).start();

                                    Post post = HomeFragment.arrayList.get(postPosition);
                                    post.setComments(post.getComments() + 1);
                                    new Thread(new UpdatePostTask(db, post)).start();

                                    HomeFragment.arrayList.set(postPosition, post);
                                    HomeFragment.recyclerView.getAdapter().notifyDataSetChanged();

                                    list.add(c);
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                    txtAddComment.setText("");

                                    String notificationRoute = Constant.COMMENT_NOTIFICATION + "/" + post.getUserId();
                                    StringRequest commentRequest = new StringRequest(Request.Method.POST, notificationRoute, response -> {
                                    }, err -> {
                                        err.printStackTrace();
                                    }) {
                                        @Override
                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                            String token = preferences.getString("token", "");
                                            HashMap<String, String> map = new HashMap<>();
                                            map.put("Authorization", "Bearer " + token);
                                            return map;
                                        }

                                    };

                                    VolleySingleton.getInstance(this).addToRequestQueue(commentRequest);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        }, error -> {
                            error.printStackTrace();
                            dialog.dismiss();
                        }) {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                String token = preferences.getString("token", "");
                                HashMap<String, String> map = new HashMap<>();
                                map.put("Authorization", "Bearer " + token);
                                return map;
                            }

                            @Nullable
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> map = new HashMap<>();
                                map.put("id", postId + "");
                                map.put("comment", commentText);
                                return map;
                            }
                        };

                        VolleySingleton.getInstance(this).addToRequestQueue(request);
                    }
                } else {
                    Toast.makeText(this, "Laad de pagina opnieuw", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(this, "Verbind met internet", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "Voer tekst in", Toast.LENGTH_SHORT).show();
        }
    }

    boolean checkConnection(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }

    public void showComments(ArrayList<Comment> comments) {
        if(comments.isEmpty()) {
            txtOfflineComments.setText("Bekijk comments eerst eenmaal online, voordat je ze offline kunt bekijken.");
        }else{
            SharedPreferences preferences = getApplicationContext().getSharedPreferences("user", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("online", false);
            editor.apply();
            this.runOnUiThread(() -> {
                adapter = new CommentsAdapter(this, comments);
                recyclerView.setAdapter(adapter);
            });
        }
    }
}