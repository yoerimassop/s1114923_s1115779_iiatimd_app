package com.example.s1114923_s1115779_iiatimd_app;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.s1114923_s1115779_iiatimd_app.Models.Comment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.GetUserPostsTask;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.UpdateCommentTask;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.UpdatePostTask;
import com.example.s1114923_s1115779_iiatimd_app.Tasks.UpdateUserTask;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditUserInfoActivity extends AppCompatActivity {
    private TextInputLayout layoutName, layoutLastName;
    private TextInputEditText txtName, txtLastname;
    private TextView textSelectPhoto;
    private Button btnSave;
    private CircleImageView circleImageView;
    private static final int GALLERY_CHANGE_PROFILE = 5;
    private Bitmap bitmap = null;
    private SharedPreferences userPref;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_info);
        init();
    }

    private void init() {
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        userPref = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        layoutLastName = findViewById(R.id.txtEditLayoutLastnameUserInfo);
        layoutName = findViewById(R.id.txtEditLayoutNameUserInfo);
        txtName = findViewById(R.id.txtEditNameUserInfo);
        txtLastname = findViewById(R.id.txtEditLastnameUserInfo);
        textSelectPhoto = findViewById(R.id.txtEditSelectPhoto);
        btnSave = findViewById(R.id.btnEditSave);
        circleImageView = findViewById(R.id.imgEditUserInfo);

        Picasso.get().load(getIntent().getStringExtra("imgUrl")).into(circleImageView);
        txtName.setText(userPref.getString("name",""));
        txtLastname.setText(userPref.getString("lastname",""));

        textSelectPhoto.setOnClickListener(v->{
            Intent i = new Intent(Intent.ACTION_PICK);
            i.setType("image/*");
            startActivityForResult(i, GALLERY_CHANGE_PROFILE);
        });

        btnSave.setOnClickListener(v->{
            if (validate()){
                boolean internet = checkConnection();
                if(internet == true) {
                    updateProfile();
                }else{
                    Toast.makeText(this, "Verbind met internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateProfile(){
        AppDatabase db = AppDatabase.getInstance(this.getApplicationContext());
        dialog.setMessage("Wijzigen...");
        dialog.show();
        String name = txtName.getText().toString().trim();
        String lastName = txtLastname.getText().toString().trim();

        StringRequest request = new StringRequest(Request.Method.POST, Constant.SAVE_USER_INFO, response->{
            try {
                JSONObject object = new JSONObject(response);
                if (object.getBoolean("success")){
                    SharedPreferences.Editor editor = userPref.edit();
                    editor.putString("name", name);
                    editor.putString("lastname", lastName);
                    editor.putString("photo", object.getString("photo"));
                    editor.apply();

                    Toast.makeText(this,"Profiel gewijzigd",Toast.LENGTH_SHORT).show();

                    int userId = userPref.getInt("id", 0);
                    new Thread(new UpdateUserTask(db, this, userId)).start();
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            dialog.dismiss();

        }, error -> {
            error.printStackTrace();
            dialog.dismiss();
        } ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = userPref.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("name", name);
                map.put("lastname", lastName);
                map.put("photo", bitmapToString(bitmap));
                return map;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==GALLERY_CHANGE_PROFILE && resultCode==RESULT_OK){
            Uri uri = data.getData();

            circleImageView.setImageURI(uri);

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validate(){
        if (txtName.getText().toString().isEmpty()){
            layoutName.setErrorEnabled(true);
            layoutName.setError("Voornaam is vereist");
            return false;
        }

        if (txtLastname.getText().toString().isEmpty()){
            layoutLastName.setErrorEnabled(true);
            layoutLastName.setError("Achternaam is vereist");
            return false;
        }
        return true;
    }

    private String bitmapToString(Bitmap bitmap) {
        if (bitmap!=null){
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte [] array = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(array, Base64.DEFAULT);
        }
        return "";
    }

    boolean checkConnection(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }

    public void updateUser(ArrayList<Post> posts, ArrayList<Comment> comments) {
        AppDatabase db = AppDatabase.getInstance(this.getApplicationContext());
        String name = userPref.getString("name", "");
        String lastname = userPref.getString("lastname", "");
        String photo = userPref.getString("photo", "");
        for ( int i = 0; i < posts.size(); i++) {
            Post post = posts.get(i);
            post.setUserName(name+ " " + lastname);
            post.setUserPhoto(photo);
            new Thread(new UpdatePostTask(db, post)).start();
        }

        for ( int i = 0; i < comments.size(); i++) {
            Comment comment = comments.get(i);
            comment.setUserName(name+ " " + lastname);
            comment.setUserPhoto(photo);
            new Thread(new UpdateCommentTask(db, comment)).start();
        }
    }
}