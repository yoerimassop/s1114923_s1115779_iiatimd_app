package com.example.s1114923_s1115779_iiatimd_app;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.s1114923_s1115779_iiatimd_app.Fragments.HomeFragment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AddPostActivity extends AppCompatActivity {

    private Button btnPost;
    private ImageView imgPost;
    private EditText txtDesc;
    private EditText txtSideDescription;
    private EditText txtTitle;

    private Bitmap bitmap = null;
    private static final int GALLERY_CHANGE_POST = 3;
    private ProgressDialog dialog;
    private SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        init();
    }

    private void init() {
        preferences = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        btnPost = findViewById(R.id.btnAddPost);
        imgPost = findViewById(R.id.imgAddPost);
        txtDesc = findViewById(R.id.txtDescAddPost);
        txtSideDescription = findViewById(R.id.txtSideDescriptionAddPost);
        txtTitle = findViewById(R.id.txtTitleAddPost);
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);

        imgPost.setImageURI(getIntent().getData());
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), getIntent().getData());
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnPost.setOnClickListener(v->{
            if(!txtTitle.getText().toString().isEmpty() && !txtDesc.getText().toString().isEmpty() && !txtSideDescription.getText().toString().isEmpty()){
                boolean internet = checkConnection();
                if(internet == true) {
                    post();
                }else{
                    Toast.makeText(this, "Verbind met internet", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(this, "Alle velden moeten worden ingevuld", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void post()
    {
        dialog.setMessage("Verzenden...");
        dialog.show();

        StringRequest request = new StringRequest(Request.Method.POST,Constant.ADD_POST, response ->{
            try {
                JSONObject object = new JSONObject(response);
                if (object.getBoolean("success")){
                    JSONObject postObject = object.getJSONObject("post");
                    JSONObject userObject = postObject.getJSONObject("user");

                    Post post = new Post();
                    post.setUserId(userObject.getInt("id"));
                    post.setUserName(userObject.getString("name") + " " +userObject.getString("lastname"));
                    post.setUserPhoto(userObject.getString("photo"));
                    post.setId(postObject.getInt("id"));
                    post.setSelfLike(false);
                    post.setPhoto(postObject.getString("photo"));
                    post.setTitle(postObject.getString("recipe_title"));
                    post.setSideDescription(postObject.getString("side_description"));
                    post.setDesc(postObject.getString("recipe_description"));
                    post.setComments(0);
                    post.setLikes(0);
                    post.setDate(postObject.getString("created_at"));

                    HomeFragment.arrayList.add(0, post);
                    HomeFragment.recyclerView.getAdapter().notifyItemInserted(0);
                    HomeFragment.recyclerView.getAdapter().notifyDataSetChanged();
                    Toast.makeText(this, "Succesvol geplaatst", Toast.LENGTH_SHORT).show();
                    finish();
                }
            } catch (JSONException e) {
                Toast.makeText(this, "Verzenden mislukt", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            dialog.dismiss();
        }, error-> {
            Toast.makeText(this, "Verzenden mislukt", Toast.LENGTH_SHORT).show();
            error.printStackTrace();
            dialog.dismiss();
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("title", txtTitle.getText().toString().trim());
                map.put("side_description", txtSideDescription.getText().toString().trim());
                map.put("description", txtDesc.getText().toString().trim());
                map.put("photo", bitmapToString(bitmap));
                return map;

            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private String bitmapToString(Bitmap bitmap) {
        if (bitmap!=null){
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte [] array = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(array, Base64.DEFAULT);
        }
        return "";
    }

    public void cancelPost(View view) {
        super.onBackPressed();
    }

    public void changePhoto(View view) {
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i, GALLERY_CHANGE_POST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_CHANGE_POST && resultCode == RESULT_OK){
            Uri imgUri = data.getData();
            imgPost.setImageURI(imgUri);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    boolean checkConnection(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }
}