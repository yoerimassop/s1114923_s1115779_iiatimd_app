package com.example.s1114923_s1115779_iiatimd_app.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

import java.util.List;

@Dao
public interface PostDAO {

    @Query("SELECT * FROM post ORDER BY id DESC")
    List<Post> getAll();

    @Query("SELECT * FROM post WHERE userId = :userId ORDER BY id DESC")
    List<Post> getUserPosts(int userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPost(Post post);

    @Update
    void updatePost(Post post);

    @Update
    void updatePosts(Post... posts);

    @Delete
    void deletePost (Post post);

}
