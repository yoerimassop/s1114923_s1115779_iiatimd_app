package com.example.s1114923_s1115779_iiatimd_app.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.s1114923_s1115779_iiatimd_app.Models.Comment;
import com.example.s1114923_s1115779_iiatimd_app.Models.Post;

import java.util.List;

@Dao
public interface CommentDAO {

    @Query("SELECT * FROM comment")
    List<Comment> getAll();

    @Query("SELECT * FROM comment WHERE postId = :postId ORDER BY id")
    List<Comment> getPostComments(int postId);

    @Query("SELECT * FROM comment WHERE userId = :userId")
    List<Comment> getUserComments(int userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertComment(Comment comment);

    @Update
    void updateComment(Comment comment);

    @Delete
    void deleteComment (Comment comment);
}

